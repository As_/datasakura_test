package my.tests.dungeons;

import java.util.*;
import java.util.stream.Collectors;

public class DungeonPool {

    final List<Dungeon> dungeons;

    public DungeonPool( List<Dungeon> dungeons ) {

        checkDungeonsPool( dungeons );

        this.dungeons = dungeons;
    }

    private void checkDungeonsPool( List<Dungeon> dungeons ){
        if( null == dungeons || dungeons.isEmpty() )
            throw new IllegalArgumentException("empty dungeons list");

        for( Dungeon dungeon : dungeons ){
            if( dungeon.entranceIdx == 0 || dungeon.exitIdx == 0 )
                throw new IllegalArgumentException("closed dungeon found");

            if( getAvailDungeons(dungeon, dungeons ).isEmpty() )
                throw new IllegalArgumentException("not matched dungeon found");
        }
    }


    public List<Dungeon> createXSequence( int length ){

        List<Dungeon> sequence = new ArrayList<>();

        if( length < 1 ){
            throw new IllegalArgumentException("incorrect x sequence length, " + length );
        }

        Dungeon dungeon = dungeons.get( new Random().nextInt(dungeons.size()) );

        sequence.add(dungeon);

        while( sequence.size() < length ){

            Dungeon last = sequence.get( sequence.size() -1 );
            List<Dungeon> availDungeons = getAvailDungeons( last , dungeons );

            if( availDungeons.isEmpty() ){
                throw new AssertionError("how could we even get here?");
            }else{
                Dungeon newLast = availDungeons.get( new Random().nextInt(availDungeons.size()));
                sequence.add( newLast );
            }

        }


        return sequence;
    }

    static List<Dungeon> getAvailDungeons( Dungeon current, List<Dungeon> dungeons ){

        return  dungeons.stream()
                .filter( d -> current.matching(d) )
                .filter( d -> !d.equals(current) )
                .collect( Collectors.toList());
    }


}
