package my.tests.dungeons;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class DungeonPoolTest {

    final Dungeon A = new Dungeon( new Dungeon.Block[][] {
            {Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR},
            {Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.AIR},
            {Dungeon.Block.AIR,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
            {Dungeon.Block.AIR,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
    });

    final Dungeon A_ = new Dungeon( new Dungeon.Block[][] {
            {Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR},
            {Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.AIR},
            {Dungeon.Block.AIR,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
            {Dungeon.Block.AIR,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
    });

    final Dungeon B = new Dungeon( new Dungeon.Block[][] {
            {Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.AIR,     Dungeon.Block.AIR,      Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.AIR},
            {Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,     Dungeon.Block.AIR,      Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR},
            {Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND,  Dungeon.Block.AIR,      Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND,  Dungeon.Block.GROUND,   Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
    });

    final Dungeon B_ = new Dungeon( new Dungeon.Block[][] {
            {Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.AIR,     Dungeon.Block.AIR,      Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.AIR},
            {Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,     Dungeon.Block.AIR,      Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR},
            {Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND,  Dungeon.Block.AIR,      Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND,  Dungeon.Block.GROUND,   Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
    });

    final Dungeon C = new Dungeon( new Dungeon.Block[][] {
            {Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.AIR,     Dungeon.Block.AIR,      Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.AIR},
            {Dungeon.Block.GROUND, Dungeon.Block.AIR,    Dungeon.Block.AIR,     Dungeon.Block.AIR,      Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR},
            {Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.GROUND,  Dungeon.Block.AIR,      Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND,  Dungeon.Block.GROUND,   Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.GROUND},
    });

    final Dungeon D = new Dungeon( new Dungeon.Block[][] {
            {Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.AIR,     Dungeon.Block.AIR,      Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.AIR},
            {Dungeon.Block.GROUND, Dungeon.Block.AIR,    Dungeon.Block.AIR,     Dungeon.Block.AIR,      Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND,    Dungeon.Block.GROUND, Dungeon.Block.GROUND,  Dungeon.Block.AIR,      Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND,  Dungeon.Block.GROUND,   Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR},
    });


    final Dungeon E = new Dungeon( new Dungeon.Block[][] {
            {Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.AIR},
            {Dungeon.Block.AIR,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
    });

    final Dungeon F = new Dungeon( new Dungeon.Block[][] {
            {Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.AIR},
            {Dungeon.Block.AIR,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
    });

    final Dungeon G = new Dungeon( new Dungeon.Block[][] {
            {Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR},
            {Dungeon.Block.AIR, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
    });

    final Dungeon H = new Dungeon( new Dungeon.Block[][] {
            {Dungeon.Block.AIR, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.AIR},
    });

    final Dungeon closed = new Dungeon( new Dungeon.Block[][] {
            {Dungeon.Block.AIR, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
            {Dungeon.Block.GROUND,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
    });


    @Test
    void illegalArgumentsPoolConstructorTest(){

        //DungeonPool не должен содержать закрытых подземелий.
        assertThrows( IllegalArgumentException.class, ()->new DungeonPool( Arrays.asList(closed,E,F,G,H)));
        //Для каждого подземелья DungeonPool'а должно быть сопоставимое.
        assertThrows( IllegalArgumentException.class, ()->new DungeonPool( Arrays.asList(B, A)));
        //Если при инициализации DungeonPool переданных подземелий будет недостаточно для генерации
        //последовательности с указанными выше условиями, должна быть выведена ошибка.
        assertThrows( IllegalArgumentException.class, ()->new DungeonPool( null ));
        assertThrows( IllegalArgumentException.class, ()->new DungeonPool( new ArrayList<>()));
        assertThrows( IllegalArgumentException.class, ()->new DungeonPool( Arrays.asList(A)));
    }

    @Test
    void correctArgumentsPoolConstructorTest(){

        DungeonPool testPool = new DungeonPool( Arrays.asList(A, B, D));
        assertEquals(testPool.dungeons.size(), 3);

        //similar and equal dungeons are not rejected
        testPool = new DungeonPool( Arrays.asList(A, B, D, D, A_));
        assertEquals(testPool.dungeons.size(), 5);

        //each has at least one matched
        testPool = new DungeonPool( Arrays.asList(E, F, G, H));
        assertEquals(testPool.dungeons.size(), 4);
    }

    @Test
    void availDungeonsTest(){

        //A has B matched
        assertEquals( DungeonPool.getAvailDungeons(A, Arrays.asList(A, B) ), Arrays.asList(B));

        // B has no matches
        assertEquals( DungeonPool.getAvailDungeons(B, Arrays.asList(A, B) ), new ArrayList<>());

        //E has just 1 of 4 matched
        assertEquals( DungeonPool.getAvailDungeons(E, Arrays.asList(E, F, G, H) ), Arrays.asList(F));

        //similar dungeons treat as equal
        assertEquals( DungeonPool.getAvailDungeons(B, Arrays.asList(C, B, B_, B) ), Arrays.asList(C) );

    }


    @Test
    void CreateXSequenceTest(){


        DungeonPool testPool = new DungeonPool( Arrays.asList(B, C) );

        //borders check
        assertThrows( IllegalArgumentException.class, ()->new DungeonPool( Arrays.asList(B, C) ).createXSequence(0) );
        assertEquals( testPool.createXSequence(1).size(), 1 );


        //cycle of 4
        testPool = new DungeonPool( Arrays.asList(E, F, G, H) );
        assertTrue( testPool.createXSequence(4).containsAll( Arrays.asList(E, F, G, H)));

        //последовательность может быть произвольной длины
        testPool = new DungeonPool( Arrays.asList(D, B, A) );
        assertEquals( testPool.createXSequence(2<<15).size(), 2<<15);

        //в последовательности не идут подряд два одинаковых подземелья
        List<Dungeon> seq = testPool.createXSequence(1000);
        for (int i = 1; i < seq.size(); i++) {
            assertNotEquals( seq.get(i-1), seq.get(i) );
        }

        //Если при выборе следующего подземелья последовательности есть выбор из нескольких сопоставимых –
        //выбирается случайное подземелье.
        assertNotEquals( testPool.createXSequence(1000), testPool.createXSequence(1000));

    }

}
