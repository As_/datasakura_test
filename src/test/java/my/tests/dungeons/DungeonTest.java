package my.tests.dungeons;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class DungeonTest {

    Dungeon.Block[][] b1 = {{Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR},
                            {Dungeon.Block.GROUND, Dungeon.Block.AIR, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.AIR,    Dungeon.Block.GROUND, Dungeon.Block.AIR},
                            {Dungeon.Block.AIR,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
                            {Dungeon.Block.AIR,    Dungeon.Block.AIR, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
    };

    Dungeon.Block[][] b2 = {{Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.AIR,     Dungeon.Block.AIR,      Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.AIR},
                            {Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,     Dungeon.Block.AIR,      Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR},
                            {Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND,  Dungeon.Block.AIR,      Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
                            {Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND,  Dungeon.Block.GROUND,   Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND},
    };
    Dungeon.Block[][] incorrectBlocks =
           {{Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.AIR,     Dungeon.Block.AIR,      Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.AIR},
            {Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR,     Dungeon.Block.AIR,      Dungeon.Block.AIR,    Dungeon.Block.AIR,    Dungeon.Block.AIR},
            {Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND,  Dungeon.Block.AIR,      Dungeon.Block.GROUND, Dungeon.Block.GROUND, Dungeon.Block.GROUND}
    };


    @Test
    void illegalArgumentsDungeonConstructorTest(){
        assertThrows( IllegalArgumentException.class, ()->new Dungeon( null ));
        assertThrows( IllegalArgumentException.class, ()->new Dungeon( incorrectBlocks ));
        assertThrows( IllegalArgumentException.class, ()->new Dungeon( new Dungeon.Block[4][7]) );
    }


    @Test
    public void testGetIdx(){

        assertEquals( new Dungeon(b1).entranceIdx, 0b0011);
        assertEquals( new Dungeon(b1).exitIdx, 0b1100);

        assertEquals( new Dungeon(b2).entranceIdx, 0b0100 );
        assertEquals( new Dungeon(b2).exitIdx, 0b1100 );


    }

    @Test
    public void testMatching(){
        assertTrue( new Dungeon(b1).matching( new Dungeon(b2)) );
        assertFalse( new Dungeon(b2).matching( new Dungeon(b1)) );

    }
}
