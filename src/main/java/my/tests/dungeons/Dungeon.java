package my.tests.dungeons;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Dungeon {

    //bit array to int, 0 - GROUND, 1 - AIR
    final int index;

    //bits, 0 - GROUND, 1 - AIR
    final int entranceIdx;
    //bits, 0 - GROUND, 1 - AIR
    final int exitIdx;
    private final Block[][] area;

    private static final int SIZE_X = 7;
    private static final int SIZE_Y = 4;


    public Dungeon(Block[][] area) {

        if( area == null || area.length != SIZE_Y ||
                Arrays.stream(area)
                        .flatMap(Arrays::stream )
                        .filter( Objects::nonNull )
                        .count() != SIZE_X*SIZE_Y)
            throw new IllegalArgumentException("wrong area in dungeon");


        this.area = area;

        entranceIdx = getEntranceIdx();
        exitIdx = getExitIdx();
        index = buildIdx();
    }

    public enum Block{
        GROUND, AIR;
    }

    public boolean matching( Dungeon next ){
        return (exitIdx & next.entranceIdx ) > 0;
    }


    private int getEntranceIdx(){
        return Stream.of(area)
                .mapToInt( a-> a[0].ordinal())//ordinal is not good, better to change enum Block to add indexes
                .reduce( (i,j) -> (i<<1) + j )
                .getAsInt();
    }

    private int getExitIdx(){
        return Stream.of(area)
                .mapToInt( a-> a[a.length-1].ordinal() )
                .reduce( (i,j) -> (i<<1) + j )
                .getAsInt();
    }


    private int buildIdx(){
         return Stream.of(area)
                .mapToInt( a-> Arrays.stream(a)
                        .mapToInt(s -> s.ordinal())
                        .reduce((i,j) -> (i<<1) + j).orElse(0))
                .reduce( (i,j) -> (i<<SIZE_X) + j )
                .getAsInt();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dungeon dungeon = (Dungeon) o;

        return index == dungeon.index;
    }

    @Override
    public int hashCode() {
        return index;
    }

    @Override
    public String toString() {
        return Stream.of(area)
                .map( a-> Arrays.stream(a)
                        .map(s -> String.valueOf(s.ordinal()))
                        .collect( Collectors.joining() ) )
                .collect(Collectors.joining("\n")) + "\n";
    }
}
